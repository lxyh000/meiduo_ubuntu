from django.db import models


# Create your models here.
class Area(models.Model):
    name = models.CharField(max_length=20, verbose_name='名称')
    # 这里写self代表自关联
    parent = models.ForeignKey("self", null=True, blank=True, related_name='subs')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '行政区划'
        verbose_name_plural = '行政区划'

    def __str__(self):
        return self.name
