from rest_framework import serializers
from .models import Area


class AreasSerializes(serializers.ModelSerializer):
    # 这里查询所有的
    class Meta:
        model = Area
        fields = ["id", "name"]


class AreaSerializes(serializers.ModelSerializer):
    # 这是查询一个的
    subs = AreasSerializes(many=True, read_only=True)

    class Meta:
        model = Area
        fields = ["id", "name", "subs"]
