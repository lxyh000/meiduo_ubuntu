from django.conf.urls import url
from . import views
from rest_framework.routers import DefaultRouter

urlpatterns = [
]

router = DefaultRouter()
router.register(r'areas', views.AreasView, base_name='areas')
urlpatterns += router.urls
# 对于包含几种请求路径的视图，需要使用这种请求方式去添加路径，它能直接添加两种路径
