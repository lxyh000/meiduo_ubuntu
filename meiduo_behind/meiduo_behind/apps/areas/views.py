from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import Area
from . import serializes


# Create your views here.
class AreasView(ReadOnlyModelViewSet):
    """因为这里只需要两种查询，所以使用readonly就可以"""
    pagination_class = None  # 是否进行分页

    # 继承之后一般只要指定查询集与查询的类
    # 因为不同的查询会有区别，所以可以可以根据查询方式进行判断
    def get_queryset(self):
        if self.action == "list":
            return Area.objects.filter(parent=None)
        else:
            return Area.objects.all()

    def get_serializer_class(self):
        if self.action == "list":
            return serializes.AreasSerializes
        else:
            return serializes.AreaSerializes
