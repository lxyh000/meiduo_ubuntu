from rest_framework import serializers
from goods.models import SKU


class CartSerializes(serializers.Serializer):
    sku_id = serializers.IntegerField()
    count = serializers.IntegerField(min_value=1)
    selected = serializers.BooleanField(default=True)

    def validate_sku_id(self, value):
        count = SKU.objects.filter(pk=value).count()
        if count <= 0:
            raise serializers.ValidationError("商品id不存在")
        return value


class CartSKUSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField()
    selected = serializers.BooleanField(label='是否勾选')

    class Meta:
        model = SKU
        fields = ["id", "count", 'default_image_url', 'price', 'selected']


class CartDeleteSerializer(serializers.Serializer):
    """
    删除购物车数据序列化器
    """
    sku_id = serializers.IntegerField()

    def validate_sku_id(self,value):
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError("商品不存在")
        return value

class CartSelectAllSerializer(serializers.Serializer):
    """
    购物车全选
    """
    selected = serializers.BooleanField(label='全选')