from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializes import CartSerializes, CartSKUSerializer,CartDeleteSerializer,CartSelectAllSerializer
from utils import myjson
from goods.models import SKU


class CartView(APIView):

    def perform_authentication(self, request):
        pass

    def post(self, request):
        # 添加购物车,首先判断是否登录

        data = request.data

        serializes = CartSerializes(data=data)
        serializes.is_valid(raise_exception=True)  # 当验证出错后,不会返回true或false会直接抛异常

        # 获取前端传过来的数据
        # 这两种方法都可以获取前端传来的值,第二种只不过是是在进行验证等一系列处理后的值,而第一种就是直接前端传来的值
        sku_id = serializes.validated_data["sku_id"]
        count = serializes.validated_data['count']
        selected = serializes.validated_data["selected"]

        response = Response(serializes.validated_data)


        try:
            user = request.user
        except:
            user = None

        if user:
            redis_conn = get_redis_connection('cart')
            p1 = redis_conn.pipeline()
            p1.hset('cart_%s' % user.id, sku_id, count)
            if selected:
                p1.sadd('cart_selected_%s' % user.id, sku_id)
            p1.execute()
            return Response(serializes.data, status=201)

        else:
            # # 如果不存在就要保存到session中
            cart_str = request.COOKIES.get('cart')
            # 判断是否存在购物车
            if not cart_str:
                cart_dict = {}
            else:
                cart_dict = myjson.loads(cart_str)
            if sku_id in cart_dict:
                count_cart = cart_dict[sku_id]["count"]
            else:
                count_cart = 0

            # 然后在购物车中添加商品
            cart_dict[sku_id] = {
                "count": count + count_cart,
                'selected': selected
            }

            cart_str = myjson.dumps(cart_dict)
            response.set_cookie('cart', cart_str, max_age=60*60*24*20)
            return response

    def get(self, request):
        """获取购物车的内容"""
        # 首先查询用户是否存在
        try:
            user = request.user
        except:
            user = None

        if user:
            redis_conn = get_redis_connection('cart')
            redis_cart = redis_conn.hgetall('cart_%s' % user.id)  # 获取次此用户所有的商品
            redis_cart_selected = redis_conn.smembers('cart_selected_%s' % user.id)  # 获取此用户所有被选中的商品
            # 从数据库中取出需要的数据,然后将数据整理好,进行序列化输出
            cart = {}
            for sku_id, count in redis_cart.items():
                cart[int(sku_id)] = {
                    'count': int(count),
                    'selected': sku_id in redis_cart_selected}
            # 上面只是弄了一个字典，并没有给对象去赋值，序列化是对对象进行序列化，所以要给对象赋值
            skus = SKU.objects.filter(id__in=cart)
            for sku in skus:
                sku.count = cart[sku.id]['count']
                sku.selected = cart[sku.id]['selected']

            serializes = CartSKUSerializer(skus, many=True)
            return Response(serializes.data)



        else:
            cart = request.COOKIES.get("cart")
            if cart:
                cart_dict = myjson.loads(cart)
            else:
                cart_dict = {}
            # 然后去遍历购物车中的数据
            skus = SKU.objects.filter(id__in=cart_dict)  # 查询字典中所有商品id
            for sku in skus:
                sku.count = cart_dict[sku.id]["count"]
                sku.selected = cart_dict[sku.id]["selected"]
            # 给每个商品的属性去赋值，然后返回给前端
            serializes = CartSKUSerializer(skus, many=True)  # 多个的时候需要设置many=True
            return Response(serializes.data)

    def put(self, request):
        data = request.data
        serializer = CartSerializes(data=data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data.get('sku_id')
        count = serializer.validated_data.get('count')
        selected = serializer.validated_data.get('selected')

        try:
            user = request.user
        except:
            user = None

        if user:
            redis_conn = get_redis_connection('cart')
            p1 = redis_conn.pipeline()
            p1.hset('cart_%s' % user.id, sku_id, count)
            if selected:
                p1.sadd('cart_selected_%s' % user.id, sku_id)
            else:
                p1.srem('cart_selected_%s' % user.id, sku_id)
            p1.execute()
            return Response(serializer.data)
        else:
            cart = request.COOKIES.get("cart")
            if not cart:
                cart_dict = {}
            else:
                cart_dict = myjson.loads(cart)

            # 因为这是进行修改,所以要进行变量的赋值然后返回
            cart_dict[sku_id] = {
                'count': count,
                'selected': selected
            }
            cart_str = myjson.dumps(cart_dict)
            response = Response(serializer.data)
            response.set_cookie('cart', cart_str, max_age=60 * 60 * 24 * 20)
            return response

    def delete(self, request):
        serializer = CartDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data['sku_id']

        try:
            user = request.user
        except:
            user = None

        if user:
            redis_conn = get_redis_connection('cart')
            p1 = redis_conn.pipeline()
            p1.hdel('cart_%s' % user.id, sku_id)
            p1.srem('cart_selected_%s' % user.id, sku_id)  # 这个没有的时候删除不会报错
            p1.execute()
            return Response(status=204)
        else:
            # 首先获取cookie中字典然后把字典中的要删除的那一项移除就好啦
            cart = request.COOKIES.get("cart")
            if not cart:
                cart_dict = {}
            else:
                cart_dict = myjson.loads(cart)

            if sku_id in cart_dict:
                del cart_dict[sku_id]

            cart_str = myjson.dumps(cart_dict)
            response = Response(status=204)
            response.set_cookie('cart', cart_str, max_age=60 * 60 * 24 * 20)
            return response


class CsrtSelectView(APIView):

    def perform_authentication(self, request):
        pass

    def put(self,request):
        # serializer = CartSelectAllSerializer(data=request.data)
        # serializer.is_valid()
        # selected = serializer.validated_data['selected']
        selected = request.data['selected']


        try:
            user = request.user
        except Exception:
            # 验证失败，用户未登录
            user = None

        if user:
            redis_conn = get_redis_connection("cart")
            # 获取所有此用户所有的商品id
            cart = redis_conn.hgetall('cart_%s' % user.id)
            keys = cart.keys()  # 获取所有的商品id，然后取修改所有商品的是否被选

            if selected:
                redis_conn.sadd('cart_selected_%s' % user.id, *keys)
            else:
                redis_conn.srem('cart_selected_%s' % user.id, *keys)
            return Response({'message': 'OK'})
        else:
            cart = request.COOKIES.get('cart')
            if not cart:
                cart_dict = {}
            else:
                cart_dict = myjson.loads(cart)
            for sku_id in cart_dict:
                cart_dict[sku_id]['selected'] = selected
            cart_str = myjson.dumps(cart_dict)
            response = Response({'message': 'OK'})
            response.set_cookie('cart', cart_str, max_age=60 * 60 * 24 * 20)
            return response





