import xadmin
from xadmin import views
from . import models


class BaseSetting(object):
    """xadmin的基本配置"""
    enable_themes = True  # 开启主题切换功能
    use_bootswatch = True

xadmin.site.register(views.BaseAdminView, BaseSetting)


class GlobalSettings(object):
    """xadmin的全局配置"""
    site_title = "美多商城运营管理系统"  # 设置站点标题
    site_footer = "美多商城集团有限公司"  # 设置站点的页脚
    menu_style = "accordion"  # 设置菜单折叠


class SKUAdmin(object):
    model_icon = 'fa fa-gift'  # 图标
    list_display = ['id', 'name', 'price', 'stock', 'sales', 'comments']  # 要显示的字段
    search_fields = ['id', 'name']  # 搜索字段
    list_filter = ['is_launched']  # 过滤器
    list_editable = ['price', 'stock']  # 可编辑的选项
    show_detail_fields = ['name']  # 可显示全部内容
    show_bookmarks = True  # 书签
    list_export = ['xls', 'csv', 'xml']  # 可导入格式
    refresh_times = [3, 5]  # 自动刷新时间
    readonly_fields = ['sales', 'comments']  # 只读字段


xadmin.site.register(views.CommAdminView, GlobalSettings)
xadmin.site.register(models.GoodsCategory)
xadmin.site.register(models.GoodsChannel)
xadmin.site.register(models.Goods)
xadmin.site.register(models.Brand)
xadmin.site.register(models.GoodsSpecification)
xadmin.site.register(models.SpecificationOption)
xadmin.site.register(models.SKU, SKUAdmin)
xadmin.site.register(models.SKUSpecification)
xadmin.site.register(models.SKUImage)