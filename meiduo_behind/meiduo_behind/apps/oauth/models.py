from django.db import models
from utils.models import BaseModel


# Create your models here.

class QQUser(BaseModel):
    """qq用户登录"""
    user = models.ForeignKey("users.User", on_delete=models.CASCADE, verbose_name="用户")
    # 这的外键和flask的完全不同，这只要写表的名字就可以，而flask还要指出哪个字段
    openid = models.CharField(max_length=64, verbose_name="openid", db_index=True)

    # db_index代表是否可以进行索引

    class Meta:
        db_table = "tb_oauth_qq"
        verbose_name = "qq用户信息"
        verbose_name_plural = verbose_name
