from django_redis import get_redis_connection
from rest_framework import serializers
from itsdangerous import TimedJSONWebSignatureSerializer
from django.conf import settings

from oauth.models import QQUser
from users.models import User


class QQBindserilaizes(serializers.Serializer):
    mobile = serializers.RegexField(regex=r'^1[3-9]\d{9}$')
    password = serializers.CharField()
    sms_code = serializers.CharField()
    access_token = serializers.CharField()

    # 进行验证
    def validate(self, attrs):
        # 这里和注册的页面是差不多的，所以要去验证短信验证码，密码，和图片验证码

        # 最重要的是要其验证openid的值
        serializer = TimedJSONWebSignatureSerializer(settings.SECRET_KEY, 600)  # 每次对象的时间最好是一样的
        # 创建对象后要进入解密或加密操作
        try:
            data = serializer.loads(attrs.get("access_token"))
            print(data)
            attrs['openid'] = data.get("openid")
        except:
            raise serializers.ValidationError("access_token无效")

        # 检验手机验证码
        sms_code = attrs.get("sms_code")
        mobile = attrs.get("mobile")
        # password = attrs.get("password")
        redis_conn = get_redis_connection('sms_code')
        real_sms_code = redis_conn.get("sms_code" + mobile)
        print(real_sms_code)
        print(sms_code)
        if not real_sms_code:
            print(11)
            raise serializers.ValidationError("验证码过期")
        redis_conn.delete("sms_code" + mobile)
        if sms_code != real_sms_code.decode():
            print(22)
            raise serializers.ValidationError("验证码错误")
        #
        # # 如果该手机号存在，则验证密码
        # try:
        #     user = User.objects.filter(mobile=mobile)
        # except User.DoesNotExist:
        #     # user = User()
        #     # user.username = mobile
        #     # user.mobile = mobile
        #     # user.set_password(password)
        #     # user.save()
        #     pass
        # else:
        #     if not user.check_password(password):
        #         raise serializers.ValidationError('密码错误')
        return attrs

    def create(self, validated_data):
        mobile = validated_data.get('mobile')
        password = validated_data.get('password')
        try:
            user = User.objects.get(mobile=mobile)
        except:
            user = User()
            user.username = mobile
            user.mobile = mobile
            user.set_password(password)
            user.save()
        else:
            # 如果有则判断密码再绑定
            if not user.check_password(password):
                raise serializers.ValidationError('此手机号已经被其它人使用')

        qquser = QQUser()
        qquser.openid = validated_data.get('openid')
        qquser.user = user
        qquser.save()

        return qquser

# filter查出来的是查询集对象，解码，单词,从redis数据库中取出的数据是字节，需要解码
