from django.shortcuts import render

# Create your views here.
from utils.merge_cart_cookie_to_redis import merge_cart_cookie_to_redis
from .qq_sdk import OAuthQQ
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .exceptions import QQAPIError
from .models import QQUser
from itsdangerous import TimedJSONWebSignatureSerializer
from django.conf import settings
from utils.jwt_token import generate_jwt_token
from .serializes import QQBindserilaizes


class QQUrlView(APIView):
    def get(self, request):
        state = request.query_params.get("next")
        oauth = OAuthQQ(state=state)
        login_url = oauth.get_qq_login_url()
        return Response({"login_url": login_url})


"""
在request对象中
request.query_params 中可以获取?param1=32&param2=23形式的参数.
request.query_params 返回的数据类型为QueryDict
QueryDict转为普通python字典. query_params.dict()即可.
"""


# 前端页面有改变的地方采取与前端交互，否则不需要与前端交互

class QQLoginBindView(APIView):
    def get(self, request):
        code = request.query_params.get('code')
        if not code:
            return Response({"message": "缺少code"}, status=status.HTTP_400_BAD_REQUEST)
        oauth = OAuthQQ()

        try:
            token = oauth.get_access_token(code)
            openid = oauth.get_openid(token)
        except QQAPIError:
            return Response({"message": "qq服务异常"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        # 获取到openid之后，判断openid是否存在
        try:
            qq_user = QQUser.objects.get(openid=openid)
        except QQUser.DoesNotExist:
            # 如果不存在证明是第一次登录
            # 返回的值要包含openid，所以要进行加密处理
            serializer = TimedJSONWebSignatureSerializer(settings.SECRET_KEY, 600)
            # serializer.dumps(数据), 返回bytes类型
            token = serializer.dumps({"openid": openid}).decode()
            return Response({
                "access_token": token
            })
        else:
            response = Response({
                "user_id": qq_user.user.id,
                "username": qq_user.user.username,
                "token": generate_jwt_token(qq_user.user)

            })
            response = merge_cart_cookie_to_redis(request, qq_user.user.id, response)
            return response

    def post(self, request):
        serializes = QQBindserilaizes(data=request.data)
        if not serializes.is_valid():
            return Response({"message": serializes.errors})
        qq_user = serializes.save()
        response = Response({
            "id": qq_user.user.id,
            "username": qq_user.user.username,
            "token": generate_jwt_token(qq_user.user),
        })
        response = merge_cart_cookie_to_redis(request, qq_user.user.id, response)
        return response
