from django.shortcuts import render
from goods.models import SKU
from datetime import datetime
from users.models import Address
from django_redis import get_redis_connection
from .models import OrderGoods
from django.db import transaction

# Create your views here.
from rest_framework import serializers

from orders.models import OrderInfo


class CartSKUSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField()
    class Meta:
        model = SKU
        fields = ['id', 'name', 'default_image_url', 'price', 'count']


class OrderSettlementSerializer(serializers.Serializer):
    freight = serializers.DecimalField(label='运费', max_digits=10, decimal_places=2)
    skus = CartSKUSerializer(many=True)


class OrderCreateSerializer(serializers.Serializer):
    address = serializers.IntegerField(write_only=True)
    pay_method = serializers.IntegerField(write_only=True)
    order_id = serializers.CharField(read_only=True)

    class Meta:
        model = OrderInfo
        fields = ['order_id', 'address', 'pay_method']


    def validate_address(self, value):
        count = Address.objects.filter(pk=value).count()
        if count <= 0:
            raise serializers.ValidationError("收获地址无效")
        return value

    def validate_pay_method(self, value):
        if value not in [1, 2]:
            raise serializers.ValidationError("付款方式无效")
        return value

    def create(self, validated_data):
        with transaction.atomic():
            # 创建保存点
            save_id = transaction.savepoint()
            # 获取当前下单用户
            user_id = self.context['request'].user.id
            # 生成订单编号
            order_id = datetime.now().strftime('%Y%m%d%H%M%S') + ('%09d' % user_id)
            address = validated_data["address"]
            pay_method = validated_data['pay_method']
            # 保存订单基本信息数据 OrderInfo
            order = OrderInfo.objects.create(
                order_id=order_id,
                user_id=user_id,
                address_id=address,
                total_count=0,
                total_amount=0,
                freight=10,
                pay_method=pay_method
            )

            # 从redis中获取购物车结算商品数据
            redis_cli = get_redis_connection("cart")
            redis_cart = redis_cli.hgetall("cart_%s" % user_id)
            cart_selected = redis_cli.smembers('cart_selected_%s' % user_id)

            cart = {}
            for sku in cart_selected:
                cart[int(sku)] = int(redis_cart[sku])
                # 这里只是得到一个字典，需要赋值到实物对象上

            # 遍历结算商品：
            skus = SKU.objects.filter(id__in=cart.keys())
            for sku in skus:
                sku_count = cart[sku.id]

                # 获取数据库中的原有库存
                origin_stock = sku.stock  # 原始库存
                origin_sales = sku.sales  # 原始销量
                print(sku.stock)

                if sku_count > origin_stock:
                    transaction.savepoint_rollback(save_id)
                    raise serializers.ValidationError("商品库存不足")

                # 判断完库存之后，进行库存修改
                new_stock = origin_stock - sku_count
                new_sales = origin_sales + sku_count
                print(sku_count)

                # sku.save()
                print(sku.stock)
                # 使用乐观锁，也就是逻辑来实现是否发生多线程，从而进行保存到数据库，如果不符合就回滚
                ret = SKU.objects.filter(pk=sku.id, stock=sku.stock).update(stock=new_stock, sales=new_sales)
                # sku.stock = new_stock
                # sku.sales = new_sales
                if ret == 0:
                    transaction.savepoint_rollback(save_id)
                    raise serializers.ValidationError("当前购买人数太多，请再次刷新")

                sku.goods.sales += sku_count
                sku.goods.save()

                # 累计订单基本信息的数据
                order.total_count += sku_count  # 累计总金额
                order.total_amount += (sku.price * sku_count)  # 累计总额

                OrderGoods.objects.create(
                    order=order,
                    sku=sku,
                    count=sku_count,
                    price=sku.price,
                )

            # 更新订单的金额数量信息
            order.total_amount += order.freight
            order.save()
            transaction.savepoint_commit(save_id)

            # 更新redis中保存的购物车数据
            pl = redis_cli.pipeline()
            pl.hdel('cart_%s' % user_id, *cart_selected)
            pl.srem('cart_selected_%s' % user_id, *cart_selected)
            pl.execute()
            return order

