from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from django_redis import get_redis_connection
from goods.models import SKU
from .serializers import OrderSettlementSerializer, OrderCreateSerializer
from rest_framework.response import Response


class OrderSettlementView(APIView):
    # 订单结算首先要用户登录
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user =request.user

        redis_cli = get_redis_connection("cart")
        key_cart = 'cart_%d' % user.id
        key_selected = "cart_selected_%d" % user.id

        # 获取数据库中的所有商品，然后获取被选中的商品
        redis_cart = redis_cli.hgetall(key_cart)
        cart_selected = redis_cli.smembers(key_selected)

        cart = {}
        for sku in cart_selected:
            cart[int(sku)] = int(redis_cart[sku])
        # 获取所有的以选择的商品
        skus = SKU.objects.filter(id__in=cart.keys())
        for sku in skus:
            sku.count = cart[sku.id]  # 给商品对象赋值
        result_dict = {
            'freight': 10,
            'skus': skus
        }
        serializer = OrderSettlementSerializer(result_dict)
        return Response(serializer.data)


class OrderCreateView(CreateAPIView):
    serializer_class = OrderCreateSerializer
    permission_classes = [IsAuthenticated]
    # 因为这里保存订单就相当于去创建一个新的订单，所有这里不需要多余的东西，就可以直接来继承，然后其余的东西写在序列化器中就可以。
