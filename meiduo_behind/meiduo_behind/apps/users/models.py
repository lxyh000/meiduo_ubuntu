from django.db import models
from django.contrib.auth.models import AbstractUser
from itsdangerous import TimedJSONWebSignatureSerializer
from django.conf import settings
from utils.models import BaseModel


# Create your models here.

class User(AbstractUser):
    # 自定义扩展用户类，在用户名、密码、邮箱等属性的基础上，扩展属性
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    email_active = models.BooleanField(verbose_name="邮箱验证状态", default=False)
    default_address = models.OneToOneField('users.Address', related_name='users', null=True, blank=True)

    class Meta:
        db_table = "tb_users"
        verbose_name = "用户"
        verbose_name_plural = verbose_name

    def generate_email_verify_url(self):
        """可以直接发送给邮箱信息，但是内容是激活信息，需要对内容进行控制，因为回调要点的链接要可以识别是哪个用户绑定的邮箱"""
        serialize = TimedJSONWebSignatureSerializer(settings.SECRET_KEY, 60 * 60 * 24)
        data = {'user_id': self.id, 'email': self.email}
        token = serialize.dumps(data).decode()
        verify_url = 'http://www.meiduo.site:8080/success_verify_email.html?token=' + token
        return verify_url

    @staticmethod
    def check_verify_email(token):
        """检验返回的token"""
        # 取出要验证的token值,因为是加密的，所以要进行解密
        serialize = TimedJSONWebSignatureSerializer(settings.SECRET_KEY, 60 * 60 * 24)
        try:
            data = serialize.loads(token)
        except:
            raise None
        else:
            email = data.get("email")
            user_id = data.get("user_id")
            try:
                user = User.objects.get(id=user_id, email=email)
            except:
                return None
            else:
                return user


class Address(BaseModel):
    user = models.ForeignKey(User, related_name="addresses")
    title = models.CharField(max_length=10)
    receiver = models.CharField(max_length=30)
    province = models.ForeignKey("areas.Area", related_name="province_address")
    city = models.ForeignKey('areas.Area', related_name='city_addr')
    district = models.ForeignKey('areas.Area', related_name='district_addr')
    place = models.CharField(max_length=50)
    mobile = models.CharField(max_length=11)
    tel = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = 'tb_address'
        verbose_name = '用户地址'
        verbose_name_plural = verbose_name
        # 排序
        ordering = ['-update_time']
