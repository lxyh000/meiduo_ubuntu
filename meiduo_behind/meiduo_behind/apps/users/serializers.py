from rest_framework import serializers
import re
from django_redis import get_redis_connection
from .models import User, Address
from rest_framework_jwt.settings import api_settings
from celery_tasks.email.tasks import send_verify_email

from django_redis import get_redis_connection
from goods.models import SKU


class AddUserBrowsingHistorySerializer(serializers.Serializer):
    # 因为课件前端传入与后端传出的都只有这一个字段,所以在这里不使用模型类序列化器,
    sku_id = serializers.IntegerField(label="商品SKU编号", min_value=1)

    # 首先进行验证这个商品的id是否存在
    def validate_sku_id(self, value):
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError("该产品不存在")
        return value

    def create(self, validated_data):
        user_id = self.context["request"].user.id
        sku_id = validated_data["sku_id"]

        redis_conn = get_redis_connection("history")
        pl = redis_conn.pipeline()

        # 移除已经存在的本商品浏览记录
        pl.lrem("history_%s" % user_id, 0, sku_id)
        # 添加新的浏览记录
        pl.lpush("history_%s" % user_id, sku_id)
        # 只保存最多5条记录
        pl.ltrim("history_%s" % user_id, 0, 4)

        pl.execute()

        return validated_data

class UserCreateSerializes(serializers.Serializer):
    token = serializers.CharField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(max_length=20, min_length=5,
                                     error_messages={
                                         'min_length': '用户名要求是5-20个字符',
                                         'max_length': '用户名要求是5-20个字符',
                                     })

    password = serializers.CharField(max_length=20, min_length=8,
                                     error_messages={
                                         "min_length": '用户名要求是5-20个字符',
                                         'max_length': '用户名要求是5-20个字符',
                                     })
    sms_code = serializers.CharField(write_only=True)
    mobile = serializers.CharField()
    password2 = serializers.CharField(write_only=True)
    allow = serializers.CharField(write_only=True)

    # 每次验证后的返回值是保存到数据库的数据
    def validate_username(self, value):
        if User.objects.filter(username=value).count():
            raise serializers.ValidationError("用户名存在")
        return value

    def validate_mobile(self, value):
        if User.objects.filter(mobile=value).count():
            raise serializers.ValidationError("手机号不存在")
        return value

    def validate_allow(self, value):
        if not value:
            raise serializers.ValidationError('必须同意协议')
        return value

    def validate(self, attrs):
        # 短信验证码
        # 连接redis数据库
        redis_cli = get_redis_connection('sms_code')
        key = 'sms_code' + attrs.get('mobile')
        # 获取
        sms_code_redis = redis_cli.get(key)
        if not sms_code_redis:
            raise serializers.ValidationError('验证码已经过期')
        redis_cli.delete(key)
        sms_code_redis = sms_code_redis.decode()
        sms_code_request = attrs.get('sms_code')
        if sms_code_redis != sms_code_request:
            raise serializers.ValidationError('验证码错误')

        # 两个密码
        pwd1 = attrs.get('password')
        pwd2 = attrs.get('password2')
        if pwd1 != pwd2:
            raise serializers.ValidationError('两次输入的密码不一致')

        return attrs

    def create(self, validated_data):
        user = User()
        user.username = validated_data.get('username')
        user.mobile = validated_data.get('mobile')
        # user.password=validated_data.get('password')
        user.set_password(validated_data.get('password'))
        # 一个对象可以拥有很多属性，保存到数据库的时候只去关心它需要的属性，其他属性它不管
        user.hh = "hh"
        user.save()

        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token

        return user


class UserDetailSerializes(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username", "mobile", "email", "email_active"]


class EmailSerializes(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "email"]

    # 重写update方法，在保存的同时去给邮箱发送验证
    def update(self, instance, validated_data):
        instance.email = validated_data.get("email")
        instance.save()

        email = validated_data["email"]
        verify_url = instance.generate_email_verify_url()
        send_verify_email.delay(email, verify_url)
        return instance


class AddressSerializes(serializers.ModelSerializer):
    # 明确定义隐藏属性
    province_id = serializers.IntegerField()
    city_id = serializers.IntegerField()
    district_id = serializers.IntegerField()

    # 将关系属性以字符串输出
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Address
        exclude = ['is_delete', 'user', 'create_time', 'update_time']

    def create(self, validated_data):
        # 外键user并没有从客户端传递过来，而是使用当前登录的用户
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)
