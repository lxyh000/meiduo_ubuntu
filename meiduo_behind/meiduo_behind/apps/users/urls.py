from django.conf.urls import url
from . import views
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework.routers import DefaultRouter

# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

urlpatterns = [
    url(r"^usernames/(?P<username>\w{5,20})/count/$", views.UserNameView.as_view()),
    url(r"^mobiles/(?P<mobile>1[3-9]\d{9})/count/$", views.MobileView.as_view()),
    url(r"^users/$", views.UserCreate.as_view()),
    # url(r"^authorizations/$", obtain_jwt_token),
    url(r'^authorizations/$', views.UserAuthorizeView.as_view()),
    url(r"^user/$", views.UserDetailView.as_view()),

    url(r"^emails/$", views.EmailView.as_view()),
    url(r'^emails/verification/$', views.VerifyEmailView.as_view()),
    url('^browse_histories/$', views.UserBrowsingHistoryView.as_view()),

]

router = DefaultRouter()
router.register("addresses", views.AddressView, base_name='addresses')
urlpatterns += router.urls
