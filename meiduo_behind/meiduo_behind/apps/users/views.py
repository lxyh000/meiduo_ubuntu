from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_jwt.views import ObtainJSONWebToken
from goods.models import SKU
from goods.serializers import SKUSerializer
from .models import User, Address
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView,ListCreateAPIView
from rest_framework.viewsets import ModelViewSet
from .serializers import UserCreateSerializes, UserDetailSerializes, EmailSerializes, AddressSerializes
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from .serializers import AddUserBrowsingHistorySerializer
from utils.merge_cart_cookie_to_redis import merge_cart_cookie_to_redis


class UserBrowsingHistoryView(ListCreateAPIView):
    """
    用户浏览历史记录
    """
    serializer_class = AddUserBrowsingHistorySerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        获取
        """
        user_id = request.user.id

        redis_conn = get_redis_connection("history")
        history = redis_conn.lrange("history_%s" % user_id, 0, 5)
        skus = []
        # 为了保持查询出的顺序与用户的浏览历史保存顺序一致
        for sku_id in history:
            sku = SKU.objects.get(id=sku_id)
            skus.append(sku)

        s = SKUSerializer(skus, many=True)
        return Response(s.data)


class UserNameView(APIView):
    def get(self, request, username):
        # 获取用户是否存在
        count = User.objects.filter(username=username).count()
        data = {
            "username": username,
            "count": count
        }
        return Response(data)


class MobileView(APIView):
    def get(self, request, mobile):
        # 获取是否存在
        count = User.objects.filter(mobile=mobile).count()
        data = {
            "mobile": mobile,
            "count": count
        }
        return Response(data)


class UserCreate(CreateAPIView):
    serializer_class = UserCreateSerializes


class UserDetailView(RetrieveAPIView):
    serializer_class = UserDetailSerializes

    # 必须登录
    # permission_classes = [IsAuthenticated]

    # 默认获取一个对象的方法为：根据主键查询
    # 当前逻辑为：哪个用户登录，则显示这个用户的信息
    def get_object(self):
        return self.request.user


class EmailView(UpdateAPIView):
    serializer_class = EmailSerializes
    # 验证用户
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user


class VerifyEmailView(APIView):
    """这些视图和序列化器要灵活使用"""

    def get(self, request):
        token = request.query_params.get('token')
        if not token:
            return Response({'message': '缺少token'}, status=status.HTTP_400_BAD_REQUEST)
        user = User.check_verify_email(token)
        if user is None:
            return Response({"message": "链接信息无效"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            # 更改数据库，进行保存
            user.email_active = True
            user.save()
            return Response({"message": "ok"})


class AddressView(ModelViewSet):
    serializer_class = AddressSerializes

    def get_queryset(self):
        return Address.objects.filter(is_delete=False)

        # retrieve====>无用
        # create===>直接使用
        # update===>直接使用

        # list

    def list(self, request, *args, **kwargs):
        addresses = self.get_queryset()
        serializer = self.get_serializer(addresses, many=True)

        return Response({
            'user_id': request.user.id,
            'default_address_id': request.user.default_address_id,
            'limit': 5,
            'addresses': serializer.data  # [{},{},...]
        })

    def destroy(self, request, *args, **kwargs):
        # 删除先取到这个对象，然后进行字段修改
        address = self.get_object()
        address.is_delete = True
        address.save()
        return Response({"message": "ok"})

    @action(methods=["put"], detail=True)
    def status(self, request, pk):
        # 设为默认就是去修改用户的属性
        # 获取当前登录的用户
        user = self.request.user
        # 修改这个用户的默认收货地址
        user.default_address_id = pk
        user.save()
        # 响应
        return Response({'message': 'OK'})

    # 修改标题===>***/pk/title/  ****/title/
    @action(methods=['put'], detail=True)
    def title(self, request, pk):
        # 接收请求报文中的标题
        title = request.data.get('title')
        # 修改对象的属性
        address = self.get_object()
        address.title = title
        address.save()
        # 响应
        return Response({'title': title})


class UserAuthorizeView(ObtainJSONWebToken):

    def post(self, request, *args, **kwargs):
        # 调用父类的方法，获取drf jwt扩展默认的认证用户处理结果
        response = super().post(request, *args, **kwargs)
        # 仿照drf jwt扩展对于用户登录的认证方式，判断用户是否认证登录成功
        # 如果用户登录认证成功，则合并购物车
        if response.status_code == 200:
            user_id = response.data.get("user_id")
            response = merge_cart_cookie_to_redis(request, user_id, response)
        return response
