from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework.views import APIView
from rest_framework.response import Response
from utils.ytx_sdk.sendSMS import CCP
from rest_framework import exceptions
from . import constans
import random
from celery_tasks.sms.tasks import send_sms_code


# url('^sms_code/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),
class SMSCodeView(APIView):
    def get(self, request, mobile):
        # 因为需要把数据存到redis数据库中，所以要获取redis连接
        redis_cli = get_redis_connection('sms_code')

        # 判断标记是否发送过短信
        sms_fag = redis_cli.get("sms_flag" + mobile)
        if sms_fag:
            raise exceptions.ValidationError("发送验证码太频繁")

        # 生成随机的验证码
        sms_code = random.randint(100000, 999999)

        # 如果是多条连接redis的命令，可以使用管道进行一次交互
        redis_pipeline = redis_cli.pipeline()
        # 保存生成的验证码到redis数据库，并设置时长，并且把标志保存到redis
        redis_pipeline.setex("sms_code" + mobile, constans.SMS_CODE_EXPIRES, sms_code)
        redis_pipeline.setex("sms_flag" + mobile, constans.SMS_FLAG_EXPIRES, 1)
        # 统一去执行
        redis_pipeline.execute()

        # 借用第三方去发送短信
        # CCP.sendTemplateSMS(mobile, sms_code, constans.SMS_CODE_EXPIRES/60, 1)
        print(sms_code)

        # 调用celery的任务：任务名.delay(参数)
        # 如果在框架中需要使用多线程，就需要使用celery去创建多线程
        # send_sms_code.delay(mobile, sms_code, constans.SMS_CODE_EXPIRES / 60, 1)
        return Response({"message": "ok"})
