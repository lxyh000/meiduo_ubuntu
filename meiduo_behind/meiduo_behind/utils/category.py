from collections import OrderedDict

from goods.models import GoodsChannel


def get_category_list():
    # 先按照组排序，同组的再按照sequence排序
    '''
    categories={分组编号：{
        'channel':一组分类,
        'sub_cats':二级分类（对象的sub_cats属性表示三级分类）
    },....}
    '''
    '''
    {
        1:{a:[],b:[]}
    }
    {
        组编号:
            [本组的一级分类],
            [本组的所有二级分类]
        组编号:[本组的一级分类],
    }
    '''
    channels = GoodsChannel.objects.order_by('group_id', 'sequence')
    categories = OrderedDict()

    for channel in channels:  #
        # {1:{一组分类,二组分类}}
        if channel.group_id not in categories:
            categories[channel.group_id] = {'channels': [], 'sub_cats': []}
        # 添加一级分类，即频道
        categories[channel.group_id]['channels'].append({
            'id': channel.id,
            'name': channel.category.name,
            'url': channel.url,
        })
        # 添加二级分类，包含三级分类
        sub_cats = channel.category.goodscategory_set.all()
        for sub in sub_cats:
            # 查询二级分类对应的三级分类
            sub.sub_cats = sub.goodscategory_set.all()
            categories[channel.group_id]['sub_cats'].append(sub)

    return categories
