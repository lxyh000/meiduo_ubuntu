from django_redis import get_redis_connection
from utils import myjson


def merge_cart_cookie_to_redis(request, user_id, response):
    # 因为需要获取cookie中的值然后要连接到redis进行数据存储，所以要传这几个参
    cookie_cart = request.COOKIES.get("cart")  # 获取cookie中的字符串数据
    if not cookie_cart:
        return response
    else:
        cookie_dict = myjson.loads(cookie_cart)  # 获取到cookie中的数据并转换为字典

    redis_cli = get_redis_connection('cart')  # 连接redis数据库
    # 获取当前用户，然后将cookie中的记录存储到redis数据库中，存储的时候遍历cookie中生成的字典
    # 2.1.1构造redis中的键
    print(user_id)
    key_user = 'cart_%d' % user_id
    key_selected = 'cart_selected_%d' % user_id
    p1 = redis_cli.pipeline()
    for key, value in cookie_dict.items():
        p1.hset(key_user, key, value["count"])
        if value["selected"]:
            p1.sadd(key_selected, key)
        else:
            p1.srem(key_selected, key)
    p1.execute()
    # 3.删除cookie中购物车数据
    response.set_cookie('cart', '', max_age=0)
    return response







