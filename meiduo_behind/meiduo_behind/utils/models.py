# 创建一个基类以便后续继承
from django.db import models


class BaseModel(models.Model):
    """为类补充基类"""
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        # 继承，用于继承，数据库迁移的时候不会创建
        abstract = True
