import pickle
import base64


# def loads(my_str):
#     json_64 = my_str.encode()  # 将字符串转化为字节（此时的字节是经过加密的）
#     json_bytes = base64.b64decode(json_64)  # 将base64的加密字节转化为字符串
#     return pickle.loads(json_bytes)  # 将一个普通的字符串转化为字典
def loads(my_str):
    # str====>bytes
    json_64 = my_str.encode()
    # b'a-zA-Z0-9'====>b'\x**\x**...'
    json_bytes = base64.b64decode(json_64)
    # bytes===>dict
    return pickle.loads(json_bytes)


# def dumps(my_dict):
#     str = pickle.dumps(my_dict)  # 将普通字典转化为普通字符串
#     str_64 = base64.b64encode(str)  # 将普通字符串进行加密转化为字节
#     return str_64.decode()  # 将字节进行解码
def dumps(my_dict):
    # b'\x**\x**...'
    json_bytes = pickle.dumps(my_dict)
    # b'a-zA-Z0-9'
    json_64 = base64.b64encode(json_bytes)
    # bytes--->字符串
    return json_64.decode()


"""
base64的解码就是解密,编码就是加密
"""